#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "DriveNode.h"

#include <rcl/rcl.h>

#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/watchdog.h"
#include "bsp/board.h"
#include "tusb.h"

#include "util/pico_uart_transports.h"

#include "SecondCore/SecondCore.h"
#include "ros/ROS.h"
#include "Config.h"
#include "Drive/driveUtils.h"
#include "util/get_serial.h"

const uint LED_PIN = 25;
bool ledState = true;

void error()
{
    for (uint8_t i = 0; i < 50; i++)
    {
        gpio_put(LED_PIN, !ledState);
        busy_wait_us(1000);
        ledState = !ledState;
    }
}

void fifo_irq_core0() {
    uint32_t flag;
    while (multicore_fifo_rvalid())
        flag = multicore_fifo_pop_blocking();

    multicore_fifo_clear_irq();
    if (flag == DRIVE_DONE_FLAG) {
        driveState = 0;
        publishDistanceDriven(stepsToDistance(stepsTaken));
    }
}

int main()
{
    board_init();
    usb_serial_init();
    tusb_init();

    /////////////////////////////////////
    // Set up Transport for Micro ROS //
    ///////////////////////////////////
    rmw_uros_set_custom_transport(
        true,
        NULL,
        pico_serial_transport_open,
        pico_serial_transport_close,
        pico_serial_transport_write,
        pico_serial_transport_read);

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    gpio_init(STEPPER_LEFT_DIRECTION);
    gpio_init(STEPPER_RIGHT_DIRECTION);
    gpio_init(STEPPER_LEFT_PULSE);
    gpio_init(STEPPER_LEFT_DIRECTION);
    gpio_init(STEPPER_ENABLE);
    gpio_init(DEBUG_BUZZER);

    gpio_set_dir(STEPPER_LEFT_DIRECTION, GPIO_OUT);
    gpio_set_dir(STEPPER_RIGHT_DIRECTION, GPIO_OUT);
    gpio_set_dir(STEPPER_LEFT_PULSE, GPIO_OUT);
    gpio_set_dir(STEPPER_LEFT_DIRECTION, GPIO_OUT);
    gpio_set_dir(STEPPER_ENABLE, GPIO_OUT);
    gpio_set_dir(DEBUG_BUZZER, GPIO_OUT);

    rcl_node_t node = rcl_get_zero_initialized_node();
    rcl_allocator_t allocator = rcl_get_default_allocator();
    rclc_support_t support;

    rclc_executor_t executor = rclc_executor_get_zero_initialized_executor();

    while (!tud_cdc_connected()) {
        tud_task();
    }

    rcl_ret_t ret = initNode(&node, &allocator, &support);
    if (ret != RCL_RET_OK)
    {
        error();
        return ret;
    }

    //ret = initPubs(&node);
    if (ret != RCL_RET_OK)
    {
        error();
        return ret;
    }
    
    ret = initSubs(&node);
    if (ret != RCL_RET_OK)
    {
        error();
        return ret;
    }
    
    ret = initExecutor(&executor, &allocator, &support);
    if (ret != RCL_RET_OK)
    {
        error();
        return ret;
    }

    gpio_put(LED_PIN, ledState);
    initMulticore();

    while (true)
    {
        ret = rclc_executor_spin_some(&executor, RCL_MS_TO_NS(100));

        if (ret != RCL_RET_OK)
        {
            error();
            return ret;
        }
    }
    return 0;
}
