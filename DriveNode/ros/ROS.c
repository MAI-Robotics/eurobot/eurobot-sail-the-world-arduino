#include <stdio.h>

#include "pico/stdio.h"
#include "hardware/gpio.h"
#include "pico/bootrom.h"
#include "rcl/rcl.h"
#include "rclc/rclc.h"
#include "rclc/executor.h"

#include "ROS.h"
#include "../DriveNode.h"
#include "../SecondCore/Restricted.h"
#include "../SecondCore/SecondCore.h"
#include "../Drive/driveUtils.h"


rcl_subscription_t distanceSubscriber;
std_msgs__msg__Int32 driveDistanceMsg;
void driveDistanceSubscriberCallback(const void *msgin)
{
    // If drive is not ready yet return
    if (driveState != 0)
        return;

    const std_msgs__msg__Int32 *msgIn = (const std_msgs__msg__Int32 *)msgin;

    DriveData data = calculateDrive(msgIn->data);
    setDriveData(data);
    multicore_fifo_push_blocking(DATA_READY_FLAG);
}

rcl_subscription_t encoderFeedbackLeftSubscriber;
std_msgs__msg__Int32 encoderFeedbackLeftMsg;
void encoderFeedbackLeftCallback(const void *msgin) {
    const std_msgs__msg__Int32 *msgIn = (const std_msgs__msg__Int32 *)msgin;
}

rcl_subscription_t encoderFeedbackRightSubscriber;
std_msgs__msg__Int32 encoderFeedbackRightMsg;
void encoderFeedbackRightCallback(const void *msgin) {
    const std_msgs__msg__Int32 *msgIn = (const std_msgs__msg__Int32 *)msgin;
}

rcl_publisher_t distanceDrivenPublisher;
std_msgs__msg__Int32 distanceDrivenMsg;
void publishDistanceDriven(int32_t distance)
{
    distanceDrivenMsg.data = distance;
    rcl_ret_t ret = rcl_publish(&distanceDrivenPublisher, &distanceDrivenMsg, NULL);
    if (ret != RCL_RET_OK) {
        error();
    }
}

rcl_subscription_t stopSubscriber;
std_msgs__msg__Empty stopMsg;
void stopSubscriberCallback(const void *msgin)
{
    multicore_fifo_push_blocking(DRIVE_INTERRUPT_FLAG);
    driveState = 2;
}

int initNode(rcl_node_t *node, rcl_allocator_t *allocator, rclc_support_t *support)
{
    // Wait for agent successful ping for 2 minutes.
    const int timeout_ms = 1000;
    const uint8_t attempts = 120;

    rcl_ret_t ret = rmw_uros_ping_agent(timeout_ms, attempts);

    if (ret != RCL_RET_OK)
    {
        // Unreachable agent, boot to bootloader
        reset_usb_boot(0, 0);
        return 0;
    }

    ret += rclc_support_init(support, 0, NULL, allocator);

    ret += rclc_node_init_default(node, "drive_node", "", support);
}

#define int32_type_support ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Int32)

#define empty_type_support ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Empty)

int initPubs(rcl_node_t *node)
{
    distanceDrivenPublisher = rcl_get_zero_initialized_publisher();

    // Initialize Publisher
    rcl_ret_t ret = rclc_publisher_init_default(
        &distanceDrivenPublisher,
        node,
        int32_type_support,
        "/feedback/drive/distance_driven");

    return ret;
}

int initSubs(rcl_node_t *node)
{
    // Initialize Subscriber for Drive Distance
    distanceSubscriber = rcl_get_zero_initialized_subscription();

    rcl_ret_t ret = rclc_subscription_init_best_effort(
        &distanceSubscriber,
        node,
        int32_type_support,
        "/drive/distance");

    // Initialize Subscriber for Stopping in Case of Collision Detected
    stopSubscriber = rcl_get_zero_initialized_subscription();

    ret += rclc_subscription_init_best_effort(
        &stopSubscriber,
        node,
        empty_type_support,
        "/drive/stop");

    encoderFeedbackLeftSubscriber = rcl_get_zero_initialized_subscription();

    ret += rclc_subscription_init_best_effort(
        &encoderFeedbackLeftSubscriber,
        node,
        int32_type_support,
        "/feedback/drive/left/processed"
    );

    encoderFeedbackRightSubscriber = rcl_get_zero_initialized_subscription();

    ret += rclc_subscription_init_best_effort(
        &encoderFeedbackRightSubscriber,
        node,
        int32_type_support,
        "/feedback/drive/right/processed"
    );

    return ret;
}

int initExecutor(rclc_executor_t *executor, rcl_allocator_t *allocator, rclc_support_t *support)
{
    ////////////////////////////////////
    // Configuration of RCL Executor //
    //////////////////////////////////

    rcl_ret_t ret = rclc_executor_init(executor, &support->context, 1, allocator);
    ret += rclc_executor_add_subscription(executor, &distanceSubscriber, &driveDistanceMsg, &driveDistanceSubscriberCallback, ON_NEW_DATA);
    ret += rclc_executor_add_subscription(executor, &stopSubscriber, &stopMsg, &stopSubscriberCallback, ON_NEW_DATA);
    ret += rclc_executor_add_subscription(executor, &encoderFeedbackLeftSubscriber, &encoderFeedbackLeftMsg, &encoderFeedbackLeftCallback, ON_NEW_DATA);
    ret += rclc_executor_add_subscription(executor, &encoderFeedbackRightSubscriber, &encoderFeedbackRightMsg, &encoderFeedbackRightCallback, ON_NEW_DATA);

    return ret;
}