#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>

#include "driveUtils.h"
#include "../util/Types.h"
#include "../util/macros.h"

DriveData calculateDrive(int distance)
{
    DriveData data;

    data.turn = 0;
    data.direction = distance > 0;
    data.steps = abs(distance) / DISTANCE_PER_STEP;

    return data;
}

inline int stepsToDistance(uint steps)
{
    return steps * DISTANCE_PER_STEP;
}