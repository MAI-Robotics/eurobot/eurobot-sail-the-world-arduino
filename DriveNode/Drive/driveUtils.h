#ifndef DRIVE_UTILS_H
#define DRIVE_UTILS_H
#ifdef __cplusplus
extern "C"
{
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>

#include "../util/Types.h"
#include "../Config.h"
#include "../util/macros.h"

// Distance around the outer perimeter of the wheel
#define OUTER_WHEEL (WHEEL_DIAMETER * M_PI)

// Gear ratio (one step on motor is x steps on wheel)
#define GEAR_RATIO MOTOR_PULLEY_TOOTH_COUNT / WHEEL_PULLEY_TOOTH_COUNT

// The distance taken per step performed
#define DISTANCE_PER_STEP (OUTER_WHEEL / STEPS_PER_MOTOR_REVOLUTION) * GEAR_RATIO

// Time needed for one step
#define TIME_PER_STEP_MS (STEPPER_INDUCTANCE * STEPPER_MAX_CURRTEN * 2) / NORMALIZED_SYSTEM_VOLTAGE
#define TIME_PER_STEP_US MS_TO_US(TIME_PER_STEP_MS)

// Mid wheel distance
#define MID_WHEEL_DISTANCE WHEEL_WIDTH + WHEEL_DISTANCE

// Angle that is performed when both motors turn at the same rate
#define ANGLE_PER_STEP 360 / ((MID_WHEEL_DISTANCE * M_PI) / DISTANCE_PER_STEP)

    /*! \brief Calculate the parameters needed for driving
 *
 * \warning ONLY CALL FROM FIRST CORE
 * \details When the distance provided is negative then the Robot will drive
 * backwards
 * \param distance The distance that needs to be driven in mm
 * \returns A DriveData struct with all needed values to execute
 */
    extern DriveData calculateDrive(int distance);

    extern int stepsToDistance(uint steps);

#ifdef __cplusplus
}
#endif
#endif // DRIVE_UTILS_H