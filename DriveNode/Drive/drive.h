#ifndef DRIVE_H
#define DRIVE_H
#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include "../SecondCore/Restricted.h"
#include "../util/Types.h"

    extern uint stepsTaken;

    // 0 = waiting for instructions, 1 = busy, 2 = halted
    extern uint driveState;

    extern volatile bool interruptDrive;

    /*! \brief Drive fast with the Data provided
 * 
 * \param data Pointer to the data used for driving
 * \warning ONLY CALL FROM SECOND CORE
 */
    void driveFast(DriveData *data);

    /*! \brief Turn fast with the Data provided
 * 
 * \param data Pointer to the data used for driving
 * \warning ONLY CALL FROM SECOND CORE
 */
    void turnFast(DriveData *data);

#ifdef __cplusplus
}
#endif
#endif // DRIVE_H