#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "pico/multicore.h"
#include "hardware/irq.h"

#include "../Config.h"
#include "../DriveNode.h"
#include "SecondCore.h"
#include "Restricted.h"
#include "../Drive/drive.h"
#include "../DriveNode.h"

bool readyToStartDrive = false;
DriveData driveData;

static int core1_loop()
{
    if (readyToStartDrive)
    {
        readyToStartDrive = false;
        multicore_fifo_push_blocking(DRIVE_BUSY_FLAG);
        driveFast(&driveData);
        multicore_fifo_push_blocking(DRIVE_DONE_FLAG);
    }
}

static void fifo_irq_core1()
{
    uint32_t flag;
    while (multicore_fifo_rvalid())
        flag = multicore_fifo_pop_blocking();

    multicore_fifo_clear_irq();

    if (flag == DRIVE_INTERRUPT_FLAG)
        interruptDrive = true;

    if (flag == DATA_MISSMATCH_FLAG)
        driveData = getDriveData();
    
    if (flag == DATA_READY_FLAG) {
        driveData = getDriveData();
        readyToStartDrive = true;
    }
}

static void core1_entry()
{
    multicore_fifo_clear_irq();
    irq_set_exclusive_handler(SIO_IRQ_PROC1, fifo_irq_core1);

    irq_set_enabled(SIO_IRQ_PROC1, true);
    while (1)
    {
        core1_loop();
    }
}

void initMulticore()
{
    critical_section_init(&drive_aim_lock);
    multicore_launch_core1(core1_entry);

    irq_set_exclusive_handler(SIO_IRQ_PROC0, fifo_irq_core0);
    irq_set_enabled(SIO_IRQ_PROC0, true);
}