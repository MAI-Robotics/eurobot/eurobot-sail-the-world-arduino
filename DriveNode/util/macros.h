#ifndef MACROS_H
#define MACROS_H
#ifdef __cplusplus
extern "C"
{
#endif

#define MS_TO_US(x) x * 1000
#define S_TO_US(x) MS_TO_US(x) * 1000

#define delay(x) busy_wait_us_32(MS_TO_US(x))

#define ABS(x) x > 0 ? x : -x

#ifdef __cplusplus
}
#endif
#endif // MACROS_H