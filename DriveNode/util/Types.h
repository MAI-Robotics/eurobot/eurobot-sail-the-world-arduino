#ifndef TYPES_H
#define TYPES_H
#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <stdbool.h>

    typedef struct
    {
        bool turn;
        uint steps;
        bool direction;
    } DriveData;

#ifdef __cplusplus
}
#endif
#endif // TYPES_H