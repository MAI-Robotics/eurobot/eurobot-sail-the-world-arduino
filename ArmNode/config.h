#ifndef CONFIG_H
#define CONFIG_H
// Pin definitions //

#define SPI_TFT_PORT spi0
#define PIN_TFT_CS 5
#define PIN_TFT_DC 1
#define PIN_TFT_RST 7
#define SPI_RX 4
#define SPI_SCK 2
#define SPI_TX 3
#define BACKLIGHT 6

// Definitons for display //

#define TFT_ENABLE_TEXT
#define TFT_ENABLE_SHAPES
#define TFT_ENABLE_ROTATE
#define TFT_ENABLE_FONTS

#endif // CONFIG_H