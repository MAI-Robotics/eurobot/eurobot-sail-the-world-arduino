#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "pico/stdlib.h"
#include "hardware/spi.h"
#include "hardware/pio.h"
#include "hardware/interp.h"
#include "hardware/timer.h"
#include "hardware/watchdog.h"
#include "hardware/clocks.h"
#include "pico/binary_info.h"

#include "pico/bootrom.h"

// TinyUSB includes for custom name
#include "bsp/board.h"
#include "tusb.h"

// Custom Micro ROS Transport
#include "usb/pico_uart_transports.h"

// We want to be able to get the unique serial of the chip
#include "usb/get_serial.h"

// Micro ROS Client Support Libraries
#include <rcl/rcl.h>
#include <rcl/error_handling.h>
#include <rclc/rclc.h>
#include <rclc/executor.h>
#include <rmw_uros/options.h>

#include "config.h"

#include "ST7735_TFT.h"

#include "tst_funcs.h"

int64_t alarm_callback(alarm_id_t id, void *user_data)
{
    // Put your timeout handler code in here
    return 0;
}

static inline uint16_t convert_rgb_to_uint16_t(int red, int green, int blue){
    uint16_t value = (((31*(red+4))/255)<<11) | 
               (((63*(green+2))/255)<<5) | 
               ((31*(blue+4))/255);
    return value;
}

void init_display()
{
    stdio_init_all();
    spi_init(spi0, 32000000); // SPI with 1Mhz
    gpio_set_function(SPI_RX, GPIO_FUNC_SPI);
    gpio_set_function(SPI_SCK, GPIO_FUNC_SPI);
    gpio_set_function(SPI_TX, GPIO_FUNC_SPI);
    bi_decl(bi_3pins_with_func(SPI_RX, SPI_TX, SPI_SCK, GPIO_FUNC_SPI));

    gpio_init(PIN_TFT_CS);
    gpio_set_dir(PIN_TFT_CS, GPIO_OUT);
    gpio_put(PIN_TFT_CS, 1); // Chip select is active-low
    bi_decl(bi_1pin_with_name(PIN_TFT_CS, "SPI CS"));

    gpio_init(PIN_TFT_DC);
    gpio_set_dir(PIN_TFT_DC, GPIO_OUT);
    gpio_put(PIN_TFT_DC, 0); // Chip select is active-low

    gpio_init(PIN_TFT_RST);
    gpio_set_dir(PIN_TFT_RST, GPIO_OUT);
    gpio_put(PIN_TFT_RST, 0);
    
    TFT_BlackTab_Initialize();
    fillScreen(ST7735_BLUE);
}

void startup_logo(){
    fillScreen(ST7735_BLACK);
    uint16_t mai_green = convert_rgb_to_uint16_t(0, 220, 0);
    drawText(0, 0, "Juju und Fabi sind super", mai_green, ST7735_BLACK, 2);
    /*
    fillRect(0, 0, 128, 128, mai_green); // background
    fillRect(0, 0, 15, 128, ST7735_BLACK); // left black stripe
    // fillRectangle();
    fillTriangle(16, 16, 16, 128, 112, 128, ST7735_WHITE); // white triangle
    fillTriangle(32, 48, 32, 80, 64, 80, mai_green); // small triangle in A
    fillRect(32, 90, 50, 29, mai_green); // lower part of A, rectangle
    fillTriangle(80, 95, 80, 128, 98, 128, mai_green); // lower part of A, triangle
    */

}

int main()
{
    // MUST BE CALLED FIRST!
    // Initialize TinyUSB
    board_init();
    usb_serial_init();
    tusb_init();

    /////////////////////////////////////
    // Set up Transport for Micro ROS //
    ///////////////////////////////////
    rmw_uros_set_custom_transport(
        true,
        NULL,
        pico_serial_transport_open,
        pico_serial_transport_close,
        pico_serial_transport_write,
        pico_serial_transport_read); 

    // END OF PRIORITY CODE

    /////////////////////////////////////
    // Initialize Inputs/Outputs here //
    ///////////////////////////////////

    stdio_init_all();

    //////////////////////////////////
    // Initialize Peripherals here //
    ////////////////////////////////

    init_display();
    gpio_init(BACKLIGHT);
    gpio_set_dir(BACKLIGHT, GPIO_OUT);
    gpio_put(BACKLIGHT, 1);

    busy_wait_ms(1000);

    fillScreen(ST7735_BLACK);

    busy_wait_ms(1000);

    drawText(10, 10, "69", ST7735_WHITE, ST7735_BLACK, 5);

    startup_logo();


    ///////////////////////////////////////
    /// END OF HARDWARE INITIALISATION ///
    /////////////////////////////////////

    ///////////////////////////
    // Initialize Micro ROS //
    /////////////////////////
    rcl_node_t node = rcl_get_zero_initialized_node();
    rcl_allocator_t allocator = rcl_get_default_allocator();
    rclc_support_t support;

    rclc_executor_t executor = rclc_executor_get_zero_initialized_executor();

    // Wait for CDC to connect
    while (!tud_cdc_connected())
    {
        tud_task();
    }

    //////////////////////////////////////////////////
    // Connect and Configurate Micro ROS Node here //
    ////////////////////////////////////////////////

    // Wait for agent successful ping for 2 minutes.
    const int timeout_ms = 1000;
    const uint8_t attempts = 120;

    rcl_ret_t ret = rmw_uros_ping_agent(timeout_ms, attempts);

    if (ret != RCL_RET_OK)
    {
        // Unreachable agent, boot to bootloader
        reset_usb_boot(0, 0);
        return 0;
    }

    ret += rclc_support_init(&support, 0, NULL, &allocator);

    ret += rclc_node_init_default(&node, "arm_node", "", &support);

    if (ret != RCL_RET_OK)
    {
        return ret;
    }

    // Publishers, Subscribers, Actions belong here

    /////////////////////////////////////
    // End of Micro ROS configuration //
    ///////////////////////////////////

    // Timer example code - This example fires off the callback after 2000ms
    add_alarm_in_ms(2000, alarm_callback, NULL, false);

    puts("Hello, world!");

    return 0;
}
